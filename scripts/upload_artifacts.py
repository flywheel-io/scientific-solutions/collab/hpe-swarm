#!/usr/bin/env python
"""Upload all file in target folder to a Flywheel project."""

import argparse
import logging
import sys
from pathlib import Path

import flywheel

log = logging.getLogger()
log.setLevel(logging.INFO)


def upload_artifacts(api_key: str, fw_project_lu: str, input_dir: Path):

    client = flywheel.Client(api_key)
    log.info(f"Finding Flywheel project {fw_project_lu}...")
    project = client.lookup(fw_project_lu)
    files = list(sorted(f for f in Path(input_dir).iterdir() if f.is_file()))
    analysis = project.add_analysis(label="HPE Swarm run")
    log.info(f"Uploading file to analysis container {analysis.label} ({analysis.id})")
    analysis.upload_output(files)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Upload all file in target folder to a Flywheel project."
    )
    parser.add_argument("--api-key", type=str, required=True, help="Flywheel API key")
    parser.add_argument(
        "--fw-project-lu",
        type=str,
        required=True,
        help="Flywheel project lookup (e.g. 'group/project')",
    )
    parser.add_argument(
        "--dir", type=str, help="Path to a directory to upload to Flywheel"
    )

    args = parser.parse_args()

    input_dir = Path(args.dir).absolute()
    if not input_dir.exists():
        log.error(f"input folder {input_dir} does not exists")
        sys.exit(1)

    # Upload artifacts to Flywheel
    upload_artifacts(args.api_key, args.fw_project_lu, input_dir)
