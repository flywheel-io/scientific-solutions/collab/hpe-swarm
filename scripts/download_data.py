#!/usr/bin/env python
import argparse
from pathlib import Path

from fw_dataset import Dataset


def download_dataset(
    api_key: str, fw_project_lu: str, output_dir: Path, datavew_label: str
):
    """Download a flywheel dataset to a local directory.

    Args:
        api_key (str): Flywheel API key.
        fw_project_lu (str): Flywheel project lookup path.
        output_dir (Path): Path to output directory.
        datavew_label (str): Datavew label.
    """
    ds = Dataset(api_key, fw_project_lu, datavew_label)
    ds.download(str(output_dir), template_path="{subject.mlset}/{file.name}")
    ds.dataframe.to_csv(
        output_dir / "dataset_df.csv"
    )  # Write out the dataframe so that it can be reused


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Download dataset")
    parser.add_argument("--api-key", type=str, required=True, help="Flywheel API key")
    parser.add_argument(
        "--fw-project-lu",
        type=str,
        required=True,
        help="Flywheel project lookup (e.g. 'group/project')",
    )
    parser.add_argument("--dataview-label", type=str, help="Datavew label")
    parser.add_argument(
        "--output-dir", type=str, help="Output directory", default="data"
    )

    args = parser.parse_args()

    # Create output directory
    output_dir = Path(args.output_dir).absolute()
    if not output_dir.exists():
        output_dir.mkdir(parents=True)

    # Download dataset
    download_dataset(
        args.api_key, args.fw_project_lu, output_dir, datavew_label=args.dataview_label
    )
