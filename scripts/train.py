#!/usr/bin/env python
import argparse
import logging
import os
import sys
from pathlib import Path

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.models as models
import torchvision.transforms.functional as TF
from PIL import Image
from pydicom import dcmread
from torch.utils import data
from torchvision import transforms
from torchvision.models.resnet import BasicBlock, ResNet
from tqdm import tqdm

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Accept the Swarm Callback parameters as Environment Variables
default_max_epochs = 5
default_min_peers = 2
default_batch_size = 8
default_sync_frequency = 16

DATA_DIR = os.getenv("DATA_DIR", "/platform/swarmml/data")
MODEL_DIR = os.getenv("MODEL_DIR", "/platform/swarmml/model")
MAX_EPOCHS = int(os.getenv("MAX_EPOCHS", str(default_max_epochs)))
SYNC_FREQUENCY = int(os.getenv("SYNC_FREQUENCY", str(default_sync_frequency)))
MIN_PEERS = int(os.getenv("MIN_PEERS", str(default_min_peers)))
BATCH_SIZE = int(os.getenv("BATCH_SIZE", str(default_batch_size)))
SWARM_ENABLED = not (os.getenv("SWARM_LOOPBACK", "False").lower() == "true")
SWARM_LOOPBACK = os.getenv("SWARM_LOOPBACK", "False").lower() == "true"
SAVE_DIR = os.path.join(MODEL_DIR, "artifacts")


class MyResNet18(ResNet):
    def __init__(self):
        super(MyResNet18, self).__init__(BasicBlock, [2, 2, 2, 2])
        self.load_state_dict(models.resnet18(pretrained=True).state_dict())
        num_ftrs = self.fc.in_features
        self.fc = nn.Linear(num_ftrs, 1)

    def forward(self, x):
        x = self._forward_impl(x)
        x = torch.sigmoid(x)
        return x.view(-1, 1).squeeze(1)


class PTDataset(data.Dataset):
    def __init__(self, paths, labels, transform=None):
        self.paths = paths
        self.labels = labels
        self.transform = transform

    def __getitem__(self, index):
        image = dcmread(self.paths[index])
        image = image.pixel_array
        image = image / 255.0
        image = (255 * image).clip(0, 255).astype(np.uint8)
        image = Image.fromarray(image).convert("RGB")
        image = transforms.ToTensor()(image)
        label = self.labels[index]
        if self.transform is not None:
            image = self.transform(image)
        return image, label

    def __len__(self):
        return len(self.paths)


def calc_validation_error(val_loader, model, criterion):
    correct = 0
    validation_loss = 0
    model.eval()

    for i, (inputs, labels) in enumerate(val_loader):
        inputs, labels = inputs.to(device), labels.to(device)
        predictions = model(inputs)
        with torch.no_grad():
            loss = criterion(predictions, labels.float())
            validation_loss += loss.data.item() * labels.size(0)

        predicted = (predictions > 0.5).float()
        correct += (labels == predicted).sum()

    val_metric = correct / len(val_loader.dataset)
    validation_loss /= len(val_loader.dataset)
    return validation_loss, val_metric


def train(data_dir: Path, num_epochs: int, swarm_enabled=False):

    model = MyResNet18()
    model.to(device)

    set_lr = 0.0003

    dataset_df = pd.read_csv(data_dir / "dataset_df.csv", index_col=[0])

    trn_file_paths = list(
        dataset_df[dataset_df["subject.mlset"] == "Training"].file_path
    )
    trn_labels = list(dataset_df[dataset_df["subject.mlset"] == "Training"].label)

    val_file_paths = list(
        dataset_df[dataset_df["subject.mlset"] == "Validation"].file_path
    )
    val_labels = list(dataset_df[dataset_df["subject.mlset"] == "Validation"].label)

    train_transform = nn.Sequential(transforms.Resize(224))
    trainset = PTDataset(trn_file_paths, trn_labels, transform=train_transform)
    train_loader = data.DataLoader(
        trainset, BATCH_SIZE, shuffle=True, num_workers=0, pin_memory=True
    )

    val_transform = nn.Sequential(transforms.Resize(224))
    valset = PTDataset(val_file_paths, val_labels, transform=val_transform)
    val_loader = data.DataLoader(
        valset, BATCH_SIZE, shuffle=False, num_workers=0, pin_memory=True
    )

    criterion = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(model.parameters(), lr=set_lr)
    lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(
        optimizer, max_lr=set_lr, steps_per_epoch=len(train_loader), epochs=num_epochs
    )

    if swarm_enabled:
        from swarmlearning.pyt import SwarmCallback

        # Model need to be provided in Swarm Callback, else Swarm Callback validation exception occurs
        # Provided Swarm_loopback, SyncFrequency, batch_size as Environment Variables
        swarmCallback = SwarmCallback(
            syncFrequency=SYNC_FREQUENCY,
            minPeers=MIN_PEERS,
            useAdaptiveSync=False,
            adsValData=valset,
            adsValBatchSize=BATCH_SIZE,
            model=model,
        )
        swarmCallback.logger.setLevel(logging.DEBUG)

    if swarm_enabled:
        swarmCallback.on_train_begin()

    log.info("Starting training...")
    for epoch in range(num_epochs):

        log.info(f"Epoch [{epoch + 1}/{num_epochs}] ")
        training_loss = 0
        model.train()
        # pbar = tqdm(total=len(train_loader), leave=False)
        for i, (inputs, labels) in enumerate(train_loader):
            inputs = inputs.to(device, non_blocking=True)
            labels = labels.to(device, non_blocking=True)
            # Imagenet statistics
            # Not sure we need, but it probably can't hurt
            normalized_batch = TF.normalize(
                inputs, mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)
            )
            outputs = model(normalized_batch)

            loss = criterion(outputs, labels.float())
            optimizer.zero_grad()

            loss.backward()
            optimizer.step()
            lr_scheduler.step()

            with torch.no_grad():
                training_loss += loss.data.item() * labels.size(0)

            # pbar.update(1)
            if swarm_enabled:
                swarmCallback.on_batch_end()
        # pbar.close()
        training_loss /= len(train_loader.dataset)
        log.info(f"Training -- Loss: {training_loss:.4f}")

        val_loss, val_accuracy = calc_validation_error(val_loader, model, criterion)
        log.info(f"Validation -- Loss: {val_loss:.4f}, Accuracy: {val_accuracy:.4f}")

        if swarm_enabled:
            swarmCallback.on_epoch_end()
    if swarm_enabled:
        swarmCallback.on_train_end()
    log.info("Training complete.")

    return model


def setup_logging(log_path: Path):
    if not log_path.parent.exists():
        log_path.parent.mkdir(parents=True)
    log = logging.getLogger()
    log.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s | %(levelname)s | %(message)s")
    file_handler = logging.FileHandler(log_path)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    log.addHandler(file_handler)
    stream = logging.StreamHandler()
    stream.setFormatter(formatter)
    log.addHandler(stream)
    return log


if __name__ == "__main__":

    parser = argparse.ArgumentParser("Training script parser")
    parser.add_argument(
        "--data-dir", help="Path to the source data folder", type=str, default=DATA_DIR
    )
    parser.add_argument(
        "--out-dir",
        help="Output folder where the training artifacts will be saved",
        type=str,
        default=SAVE_DIR,
    )
    parser.add_argument(
        "--num-epoch", help="Number of epochs", type=int, default=MAX_EPOCHS
    )
    parser.add_argument(
        "--no-swarm", help="Disable swarm callbacks", action="store_false"
    )

    args = parser.parse_args()
    data_dir = Path(args.data_dir)
    output_folder = Path(args.out_dir)
    output_folder.mkdir(parents=True, exist_ok=True)

    # Configure logger
    log = setup_logging(output_folder / "logs.log")

    if not data_dir.exists():
        log.error(f"{data_dir} does not exist. Exiting")
        sys.exit(1)
    swarm_enabled = False if args.no_swarm is False else SWARM_ENABLED

    model = train(data_dir, args.num_epoch, swarm_enabled=swarm_enabled)

    torch.save(model.state_dict(), output_folder / "model.pt")
