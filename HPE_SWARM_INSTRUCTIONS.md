# Create Flywheel example

*   git clone Swarm learning Repo (git@github.com:HewlettPackard/swarm-learning.git)
*   Create directory 'flywheel' in `swarm-learning/examples/`
* 	Copy the content from `/swarm-learning/examples/cifar10` into `/swarm-learning/examples/flywheel`
*   Replace cifar10.py with Flywheel train.py in `/swarm-learning-docs/examples/flywheel/model`
*   Put Flywheel Raw-data in `/swarm-learning-docs/examples/flywheel/app-data/fl<n>-node`(n = 1, 2), also need make sure that dataset_df.csv paths should point
    to the user container data mount path `/tmp/test/app-data/fl<n>-node` (mount happens in point 6 and 7 below)   
*   Replace requirements.txt in `/swarm-learning-docs/examples/flywheel/ml-context` with Flywheel requirements.txt
*   This example assumes that License Server already runs on HOST. All Swarm nodes connect to the License Server, on its default port 5814.

## Running Flywheel example

Flywheel example is executed on Single Host. This example uses one SN node, SN1 is the name of the Docker container. Two SL and ML nodes  are manually spawned by running the run-sl script. Swarm training gets invoked once ML nodes are started. Name of the SL nodes that runs as container are SL1 and SL2. Name of the ML nodes that runs as container are ML1 and ML2.

1.  Navigate to swarm-learning folder (that is, parent to examples directory).
    ```bash
    cd swarm-learning
    ```
2.  Create a temporary workspace directory and copy `flywheel` example.
    ```bash
    mkdir workspace
	cp -r examples/flywheel workspace/
    cp -r examples/utils/gen-cert workspace/flywheel/
    ```
3.  Run the gen-cert utility to generate certificates for each Swarm component using the command `gen-cert -e <EXAMPLE-NAME> -i <HOST-INDEX>`
    ```bash
    ./workspace/flywheel/gen-cert -e flywheel -i 1
	./workspace/flywheel/gen-cert -e flywheel -i 2
    ```
4.  Build Docker image for ML that contains environment to run Swarm training of user models.
    ```bash
    cp -L lib/swarmlearning-client-py3-none-manylinux_2_24_x86_64.whl workspace/flywheel/ml-context/
	docker build -t user-ml-pyt:flywheel --build-arg https_proxy=http://<your-proxy-server-ip>:<port> workspace/flywheel/ml-context
    ```
5.  Run Swarm Network node (sentinel node)
    ```bash
    ./scripts/bin/run-sn -d --name=sn1 --host-ip=<HOST IP> --sentinel --sn-api-port=30304 \
	--key=workspace/flywheel/cert/sn-1-key.pem --cert=workspace/flywheel/cert/sn-1-cert.pem \
	--capath=workspace/flywheel/cert/ca/capath --apls-ip=<HOST IP>
    ```
6.  Run Swarm Learning node sl1 and Machine Learning node ml1
    ```bash
    ./scripts/bin/run-sl --name=sl1 --host-ip=<HOST IP> \
	--sn-ip=<HOST IP> --sn-api-port=30304 --sl-fs-port=16000 \
	--key=workspace/flywheel/cert/sl-1-key.pem \
	--cert=workspace/flywheel/cert/sl-1-cert.pem \
	--capath=workspace/flywheel/cert/ca/capath \
	--ml-image=user-ml-pyt:flywheel --ml-name=ml1 \
	--ml-w=/tmp/test --ml-entrypoint=python3 --ml-cmd=model/train.py \
	--ml-v=workspace/flywheel/model:/tmp/test/model \
	--ml-v=workspace/flywheel/app-data/fl1-node:/tmp/test/app-data/fl1-node \
	--ml-e DATA_DIR=app-data/fl1-node/raw-data --ml-e MODEL_DIR=model \
	--ml-e MAX_EPOCHS=10 --ml-e SYNC_FREQUENCY=4 \
	--ml-e MIN_PEERS=2 --ml-e BATCH_SIZE=4 \
	--ml-e SWARM_LOOPBACK=false \
	--ml-e https_proxy=http://<your-proxy-server-ip>:<port> \
	--apls-ip=<HOST IP>
    ```
7.  Run Swarm Learning node sl2 and Machine Learning node ml2
    ```bash
    ./scripts/bin/run-sl --name=sl2 --host-ip=<HOST IP> \
	--sn-ip=<HOST IP> --sn-api-port=30304 --sl-fs-port=17000 \
	--key=workspace/flywheel/cert/sl-2-key.pem \
	--cert=workspace/flywheel/cert/sl-2-cert.pem \
	--capath=workspace/flywheel/cert/ca/capath \
	--ml-image=user-ml-pyt:flywheel --ml-name=ml2 \
	--ml-w=/tmp/test --ml-entrypoint=python3 --ml-cmd=model/train.py \
	--ml-v=workspace/flywheel/model:/tmp/test/model \
	--ml-v=workspace/flywheel/app-data/fl2-node:/tmp/test/app-data/fl2-node \
	--ml-e DATA_DIR=app-data/fl2-node/raw-data --ml-e MODEL_DIR=model \
	--ml-e MAX_EPOCHS=10 --ml-e SYNC_FREQUENCY=4 \
	--ml-e MIN_PEERS=2 --ml-e BATCH_SIZE=4 \
	--ml-e SWARM_LOOPBACK=false\
	--ml-e https_proxy=http://<your-proxy-server-ip>:<port> \
	--apls-ip=<HOST IP>
    ```
